var path = require('path')
var KraftAPI = require('../api/kraft/')
var PIMWorkbook = require('./utils/PIMWorkbook.js')
var workbook = new PIMWorkbook()
var importRecipes = require('./importRecipes.js')
var importProducts = require('./importProducts.js')


function importKraftData(config,cb){
	config = Object.assign({
		brands:[],
		products:[],
		recipes:[],
		filename:'export_'+new Date().getTime()+'_'+Math.floor(10+Math.random()*89),
		options:{}
	},config)

	if(config.brands.length>0){
		// add brand products to config.products
		KraftAPI.getBrandProducts(config.brands.pop(),function(brandProductsStr){

			let brandProducts = JSON.parse(brandProductsStr)

			config.products = config.products.concat(brandProducts.Products.map(function(product){
				return product.GTIN
			}))
			// recurse this function
			importKraftData(config,cb)
		},config.options)
		return
	}

	// add recipes first because they have products embedded in them
	importRecipes.addRecipes(config.recipes, workbook, function(recipes, subproductids){
		config.products = config.products.concat(subproductids)
		// recipes contain related products - add those also
		importProducts.addProducts(config.products, workbook, function(products){

			let filepath = path.join(__dirname,'exports/',config.filename+'.xlsx')

			workbook.save(filepath,function(){
				cb({
					logs:[
						'Added '+recipes.length+'/'+config.recipes.length+' Recipes',
						'Added '+products.length+'/'+config.products.length+' Products'
					],
					filepath:filepath
				})
				console.log('import to XLS complete')
			})
			
		},config.options)
	})
}

//
// example request call
//

// importKraftData({
// 	brands:["50"],
// 	products:["00021000006687","00029000015623"],
// 	recipes:["200835", "200489", "200493", "200487", "200486"]
// },function(info){
// 	console.log(JSON.stringify(info,null,2))
// })

module.exports = {
	importKraftData:importKraftData
}