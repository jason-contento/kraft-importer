// var KraftAPI = require('../api/kraft/')
// var RecipeSchema = require('./KraftRecipesSchema.js')

// function addRecipes(recipeIds, workbook, cb){
// 	let subproductids = []
// 	importRecipes(recipeIds, function(recipes){
// 		recipes.map(function(recipe){
// 			if(recipe && recipe.RecipeId){
// 				let productids = addRecipe(recipe,workbook)
// 				// we need to collect the related products for import 
// 				subproductids = subproductids.concat(productids)
// 			}
// 		})
// 		if(cb) cb(recipes,subproductids)
// 	})
// }

// function importRecipes(recipeIds, cb, result){
// 	KraftAPI.getRecipes(recipeIds, function(response){
// 		cb(JSON.parse(response).Recipes)
// 	})
// }

function cleanupRecipe(recipe){

	console.log('cleanupRecipe:',recipe.RecipeId)

	//
	// Product Master
	//

	//workbook.insertRows('productmaster',recipe,RecipeSchema)

	//
	// CATEGORIES
	//
	let PrimaryCategory = ''

	if(recipe.Categories && recipe.Categories.length>0){
		let primaryCategories = recipe.Categories.filter(function(category){
			return category.CategoryName==='Primary'
		})

		if(primaryCategories.length===0 && recipe.Categories.length>0){
			//primaryCategories = [recipe.Categories[0]]
			PrimaryCategory = recipe.Categories[0].CategoryName
		} else {
			PrimaryCategory = primaryCategories[0].SubCategoryName
		}
	}

	let assets = []
	let w

	recipe.Assets.map(function(pic){
		var q0 = pic.imageType==='Primary' ? 1 : 0
		pic.PictureFormats.map(function(fmt){
			//w = parseInt(fmt.url.split('_').pop().split('.')[0])

			if(fmt.url.match(/(\d*x\d*)/)){
				w = parseInt(fmt.url.split('_').pop().split('.')[0].split('x')[0])
			} else {
				w = parseInt(fmt.rendition.split('x')[0])
			}
			//var w = parseInt(fmt.rendition.split('x')[0])
			var q1 = Math.abs(640-w)
			assets.push({url:fmt.url, w:w, q0:q0, q1:q1, r:fmt.rendition})
		})
	})

	assets.sort(function(a,b){
		return a.q0>b.q0 ? -1 : a.q0<b.q0 ? 1 : (a.q1<b.q1 ? -1 : a.q1>b.q1 ? 1 : 0)
	})

	// if(assets[0].url.indexOf('960')>-1){
	// 	console.log('Assets',assets)
	// }

	//
	// Nutrients
	//

	let nutritionMap = {
		'Calories':'nutrition_calories',
		'Protein':'nutrition_protein_amt',
		'Carbohydrate':'nutrition_carbohydrates_amt',
		'Dietary fiber':'nutrition_fiber_amt',
		'Sugars':'nutrition_sugars_amt',
		'Total fat':'nutrition_fat_amt',
		'Saturated fat':'nutrition_saturatedfat_amt',
		'Monounsaturated Fat':'nutrition_monounsaturatedfat_amt',
		'Polyunsaturated Fat':'nutrition_polyunsaturatedfat_amt',
		'Trans Fat':'nutrition_transfat_amt',
		'Cholesterol':'nutrition_cholesterol_amt',
		'Vitamin A':'nutrition_vitamina_dv',
		'Vitamin C':'nutrition_vitaminc_dv',
		'Vitamin D':'nutrition_vitamind_dv',
		'Vitamin E':'nutrition_vitamine_dv',
		'Calcium':'nutrition_calcium_dv',
		'Iron':'nutrition_iron_dv',
		'Potassium':'nutrition_potassium_amt',
		'Sodium':'nutrition_sodium_amt',
		'Added Sugars':'nutrition_addedsugars_amt'
	}

	let nutrition = {}

	recipe.NutritionItems.map(function(item){
		if(item.NutritionItemName in nutritionMap){
			nutrition[nutritionMap[item.NutritionItemName]] = item.Quantity+(item.Unit.indexOf('%DV')>-1 ? '%' : item.Unit.trim())
		} else {
			console.log('New Nutrition Item:',recipe.RecipeId,item.NutritionItemName)
		}
	})

	//
	// Properties
	//

	//workbook.insertRows('productmasterproperties',[
	return {
		RecipeId:recipe.RecipeId,
		Name:recipe.RecipeName,
		Tips:recipe.Tips ? recipe.Tips.map(function(tip){
			return {
				title:tip.TipName,
				description:tip.TipDescription
			}
			return tip
		}) : '',
		hasVideo:Boolean(recipe.HasVideo),
		Video:recipe.Video ? JSON.stringify(recipe.Video) : '',
		videoData:recipe.Video ? JSON.stringify(recipe.Video) : '',
		Image:assets && assets.length>0 ? assets[0].url : '',
		Assets:assets,
		Ingredients:recipe.Ingredients.map(function(ingredient){
			return `${ingredient.FullMeasure || ingredient.QuantityText} ${ingredient.PrePreparation} ${ingredient.IngredientName} ${ingredient.PostPreparation}`
		}),
		PreparationSteps:recipe.PreparationSteps.map(function(prep){
			return prep.Description
		}),
		PreparationTime:recipe.PreparationTime,
		Yield:recipe.Yield,
		TotalTime:recipe.TotalTime,
		PrimaryCategory:PrimaryCategory
	}


	//
	// SKUs
	//

	//workbook.insertRows('skumaster',recipe,RecipeSchema)

	//
	// get collecton product gtins
	//
	// let subproductids = recipe.RiseIngredients
	// .filter(function(ingredient){
	// 	return ingredient.ProductId
	// }).map(function(ingredient){
	// 	return ingredient.ProductId
	// })

	// return subproductids
}

module.exports = {
	cleanup:cleanupRecipe,
	cleanupRecipe:cleanupRecipe
}