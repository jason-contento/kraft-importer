var LogIt = (function(){

	var $el = $('<pre class="form-control" id="log"></pre>')
	var log = []

	$el.css({
		backgroundColor: '#333',
		color: '#dea549'
	})

	function render(){
		$el.text(log.map(function(ln){
			if(typeof ln == 'function'){
				return ln()
			} else {
				return ln
			}
		}).join('\n'))
	}

	function insert(str,ln){
		ln = !isNaN(ln) ? ln : log.length
		log[ln] = str
		render()
		return ln
	}

	return {
		$el:$el,
		render:render,
		insert:insert
	}

})()