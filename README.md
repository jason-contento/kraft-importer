# kraft-importer

This project imports Kraft products and recipes from their API. This was written to aid in importing the data into Skava PIM.

## Getting Started

Launch the Express server:
```
$ npm run dev
```


Note: The interface caches the responses indefinitely, so you'll need to manually empty the api/cache folder to get fresh responses.