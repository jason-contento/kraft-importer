var KraftAPI = require('../api/kraft/')
var RecipeSchema = require('./KraftRecipesSchema.js')

function addRecipes(recipeIds, workbook, cb){
	let subproductids = []
	importRecipes(recipeIds, function(recipes){
		recipes.map(function(recipe){
			if(recipe && recipe.RecipeId){
				let productids = addRecipe(recipe,workbook)
				// we need to collect the related products for import 
				subproductids = subproductids.concat(productids)
			}
		})
		if(cb) cb(recipes,subproductids)
	})
}

function importRecipes(recipeIds, cb, result){
	KraftAPI.getRecipes(recipeIds, function(response){
		cb(JSON.parse(response).Recipes)
	})
}

function addRecipe(recipe, workbook){

	console.log('addRecipe:',recipe.RecipeId)

	//
	// Product Master
	//

	workbook.insertRows('productmaster',recipe,RecipeSchema)

	//
	// CATEGORIES
	//
	let PrimaryCategory = ''

	if(recipe.Categories && recipe.Categories.length>0){
		let primaryCategories = recipe.Categories.filter(function(category){
			return category.CategoryName==='Primary'
		})

		if(primaryCategories.length===0 && recipe.Categories.length>0){
			//primaryCategories = [recipe.Categories[0]]
			PrimaryCategory = recipe.Categories[0].CategoryName
		} else {
			PrimaryCategory = primaryCategories[0].SubCategoryName
		}
	}

	var assets = []

	recipe.Assets.map(function(pic){
		var q0 = pic.imageType==='Primary' ? 1 : 0
		pic.PictureFormats.map(function(fmt){
			var q1 = 1024-parseInt(fmt.rendition.split('x')[0])
			assets.push({url:fmt.url, q0:q0, q1:q1})
		})
	})

	assets.sort(function(a,b){
		a.q0>b.q0 ? -1 : a.q0<b.q0 ? 1 : a.q1<b.q1 ? -1 : a.q1>b.q1 ? 1 : 0
	})

	//
	// Properties
	//

	workbook.insertRows('productmasterproperties',[
		{
			name:'Name',
			value:recipe.RecipeName
		},
		{
			name:'isRecipe',
			value:true
		},
		{
			name:'Tips',
			value:recipe.Tips ? recipe.Tips.map(function(tip){
				// make tips backwards compatible
				tip.name = tip.TipName
				tip.desc = tip.TipDescription
				return tip
			}) : ''
		},
		{
			name:'entryType',
			value:'recipe'
		},
		{ // to be retired
			name:'hasVideo',
			value:Boolean(recipe.HasVideo)
		},
		{
			name:'HasVideo',
			value:Boolean(recipe.HasVideo)
		},
		{
			name:'Video',
			value:recipe.Video ? JSON.stringify(recipe.Video) : ''
		},
		{ // to be retired
			name:'videoData',
			value:recipe.Video ? JSON.stringify(recipe.Video) : ''
		},
		{
			name:'Image',
			value:assets.length>0 ? assets[0].url : ''
		},
		{ // to be retired
			name:'RecipeIngredients',
			value:recipe.Ingredients.map(function(ingredient){
				return `${ingredient.FullMeasure || ingredient.QuantityText} ${ingredient.PrePreparation} ${ingredient.IngredientName} ${ingredient.PostPreparation}`
			})
		},
		{
			name:'Ingredients',
			value:recipe.Ingredients
		},
		{
			name:'RecipePreparation',
			value:recipe.PreparationSteps.map(function(prep){
				return prep.Description
			})
		},
		{
			name:'PreparationSteps',
			value:recipe.PreparationSteps.map(function(prep){
				return prep.Description
			})
		},
		{
			name:'PreparationTime',
			value:recipe.PreparationTime
		},
		{
			name:'Yield',
			value:recipe.Yield
		},
		{
			name:'TotalTime',
			value:recipe.TotalTime
		},
		{
			name:'PrimaryCategory',
			value:PrimaryCategory
		}
	].map((prop)=>{
		// apply same productid to each property
		return Object.assign(prop,{productid:recipe.RecipeId})
	}),RecipeSchema)

	//
	// nutrition
	//

	let nutritionMap = {
		'Calories':'nutrition_calories',
		'Protein':'nutrition_protein_amt',
		'Carbohydrate':'nutrition_carbohydrates_amt',
		'Dietary fiber':'nutrition_fiber_amt',
		'Sugars':'nutrition_sugars_amt',
		'Total fat':'nutrition_fat_amt',
		'Saturated fat':'nutrition_saturatedfat_amt',
		'Monounsaturated Fat':'nutrition_monounsaturatedfat_amt',
		'Polyunsaturated Fat':'nutrition_polyunsaturatedfat_amt',
		'Trans Fat':'nutrition_transfat_amt',
		'Cholesterol':'nutrition_cholesterol_amt',
		'Vitamin A':'nutrition_vitamina_dv',
		'Vitamin C':'nutrition_vitaminc_dv',
		'Vitamin D':'nutrition_vitamind_dv',
		'Vitamin E':'nutrition_vitamine_dv',
		'Calcium':'nutrition_calcium_dv',
		'Iron':'nutrition_iron_dv',
		'Potassium':'nutrition_potassium_amt',
		'Sodium':'nutrition_sodium_amt',
		'Added Sugars':'nutrition_addedsugars_amt'
	}

	let nutrition = {}

	recipe.NutritionItems.map(function(item){
		if(item.NutritionItemName in nutritionMap){
			workbook.insertRows('productmasterproperties',{
				name:nutritionMap[item.NutritionItemName],
				value:item.Quantity+(item.Unit.indexOf('%DV')>-1 ? '%' : item.Unit.trim()),
				productid:recipe.RecipeId
			})

		} else {
			console.log('New Nutrition Item:',recipe.RecipeId,item.NutritionItemName)
		}
	})
	/*

	let nutritionMap = {
		371:'nutrition_calories',
		372:'nutrition_protein_amt',
		373:'nutrition_carbohydrates_amt',
		374:'nutrition_fiber_amt',
		375:'nutrition_sugars_amt',
		376:'nutrition_fat_amt',
		377:'nutrition_saturatedfat_amt',
		378:'nutrition_monounsaturatedfat_amt',
		379:'nutrition_polyunsaturatedfat_amt',
		380:'nutrition_transfat_amt',
		381:'nutrition_cholesterol_amt',
		382:'nutrition_vitamina_dv',
		383:'nutrition_vitaminc_dv',
		384:'nutrition_vitamind_dv',
		385:'nutrition_vitamine_dv',
		386:'nutrition_calcium_dv',
		387:'nutrition_iron_dv',
		388:'nutrition_potassium_amt',
		389:'nutrition_sodium_amt',
		390:'nutrition_addedsugars_amt'
	}

	recipe.NutritionItems.map(function(item){
		if(item.NutritionItemID in nutritionMap){
			workbook.insertRows('productmasterproperties',{
				name:nutritionMap[item.NutritionItemID],
				value:item.Quantity+(item.Unit.indexOf('%DV')>-1 ? '%' : item.Unit.trim()),
				productid:recipe.RecipeId
			})
		} 
	})
	*/

	//
	// CATEGORIES
	//

	// let primaryCategories = recipe.Categories.filter(function(category){
	// 	return category.CategoryName==='Primary'
	// })

	// if(primaryCategories.length===0 && recipe.Categories.length>0){
	// 	primaryCategories = [recipe.Categories[0]]
	// }

	workbook.insertRows('productcategory',{
		categoryid: 'recipes',
		productid: recipe.RecipeId
	},RecipeSchema)

	//
	// SKUs
	//

	workbook.insertRows('skumaster',recipe,RecipeSchema)

	//
	// get collecton product gtins
	//
	let subproductids = recipe.RiseIngredients
	.filter(function(ingredient){
		return ingredient.ProductId
	}).map(function(ingredient){
		return ingredient.ProductId
	})

	return subproductids
}

module.exports = {
	addRecipes:addRecipes
}