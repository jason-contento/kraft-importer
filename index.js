const express = require('express')
const fs = require('fs')
const path = require('path')
const Freemarker = require('freemarker.js')
const app = express()
const openport = require('openport')
const opn = require('opn')
// var exec = require('child_process').exec;
// var KraftProduct = require('./KraftProduct')
// var KraftBrands = require('./KraftProduct/KraftBrands.js')
// const Promise = require("bluebird")
const KraftAPI = require('./api/kraft/')
// const ExportPIM = require('./ExportPIM/')
const bodyParser = require('body-parser')
const toXLS = require('./toXLS/')
const ProductCleaner = require('./toXLS/KraftProductCleanup.js')
const RecipeCleaner = require('./toXLS/KraftRecipeCleanup.js')

/*

Helper

*/

var fm = new Freemarker({
	viewRoot: __dirname+'/freemarker',
	options: {}
})

app.set('port', (process.env.PORT || 5002))
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//
// API JSON Responses
//

app.get('/api/json/recipes/', function(req, res) {

	KraftAPI.getRecipes(req.query.recipes, function(data){
		var recipes = JSON.parse(data)
		var result = []
		recipes.Recipes.map(function(recipe){
			result.push(RecipeCleaner.cleanupRecipe(recipe))
		})
		res.end(JSON.stringify(result,null,2))
	},{nocache:req.query.nocache || false})
})

app.get('/api/kraft/recipes/:recipeId', function(req, res) {
	KraftAPI.getRecipes(req.params.recipeId, function(data){
		res.end(data)
	},{nocache:req.query.nocache || false})
})

app.get('/api/kraft/recipes/search', function(req, res) {
	KraftAPI.getRecipeRelated({ProductName:req.params.query}, function(data){
		res.end(data)
	},{nocache:req.query.nocache || false})
})

app.get('/api/kraft/brands', function(req, res) {
	res.setHeader('Content-Type', 'application/json')
	if(req.query.products){
		KraftAPI.getBrandsWithProducts(function(jsonResponse){
			res.end(jsonResponse)
		})
	} else {
		KraftAPI.getBrands(function(jsonResponse){
			res.end(jsonResponse)
		})
	}
})

app.get('/api/kraft/brands/:brandId', function(req, res) {
	res.setHeader('Content-Type', 'application/json')
	if(req.query.hydrated){
		KraftAPI.getBrandProductsHydrated(req.params.brandId, function(productsResp){
			res.end(productsResp)
		})
	} else {
		KraftAPI.getBrandProducts(req.params.brandId, function(productsResp){
			res.end(productsResp)
		})
	}
})

app.get('/api/kraft/product/:productId', function(req, res) {
	if(req.params.productId.indexOf(',')>-1){
		KraftAPI.getProducts(req.params.productId, function(productsResp){
			if(req.query.clean==='1'){
				const ProductCleaner = require('./toXLS/KraftProductCleanup.js')
				let data = JSON.parse(productsResp)
				let products = data.Products.map(function(product){
					return ProductCleaner.cleanup(product).__properties
				})
				productsResp = JSON.stringify({entries:products},null,2)
			}
			res.end(productsResp)
		})
	} else {
		KraftAPI.getProduct(req.params.productId, function(productsResp){
			if(req.query.clean==='1'){
				const ProductCleaner = require('./toXLS/KraftProductCleanup.js')
				productsResp = JSON.stringify(ProductCleaner.cleanup(JSON.parse(productsResp)).__properties,null,2)
			}
			res.end(productsResp)
		})
	}
})

//
// UI
//

app.get('/ui', function(req,res){
	let html = fm.renderSync('iframes.html', {})
	let pageHTML = fm.renderSync('framework.html', {title:'Kraft Brands', body:html})
	res.end(pageHTML)
})

app.get('/ui/kraft/brands', function(req, res) {
	console.log('ui/kraft/brands')
	KraftAPI.getBrands(function(brandsResp){
		let brandsHTML = fm.renderSync('brands.html', JSON.parse(brandsResp))
		let pageHTML = fm.renderSync('framework.html', {title:'Kraft Brands', body:brandsHTML})
		//console.log('brandsHTML',brandsHTML)
		//console.log('pageHTML',pageHTML)
		if(typeof pageHTML==='string'){
			res.end(pageHTML)
		} else {
			res.end(brandsResp)
		}
	})
})

app.get('/ui/kraft/brands/:brandId', function(req, res) {
	KraftAPI.getBrandProducts(req.params.brandId, function(brandsResp){
		let brandsHTML = fm.renderSync('brand_products.html', JSON.parse(brandsResp))
		let pageHTML = fm.renderSync('framework.html', {title:'Kraft Brands', body:brandsHTML})
		if(typeof pageHTML==='string'){
			res.end(pageHTML)
		} else {
			res.end(brandsResp)
		}
	})
})

app.get('/ui/kraft/product/:productId', function(req, res) {
	KraftAPI.getProduct(req.params.productId, function(productsResp){
		let data = ProductCleaner.cleanup(JSON.parse(productsResp))
		console.log('data',data)
		let productHTML = fm.renderSync('product-nutrition.html', data)

		if(typeof productHTML!=='string'){
			return res.end('Error: product-nutrition.html'+JSON.stringify(data.__properties,null,2))
		}

		let pageHTML = fm.renderSync('framework.html', {title:'Kraft Product', body:productHTML || 'hello'})
		
		if(typeof pageHTML==='string'){
			res.end(pageHTML)
		} else {
			res.end('Error')
		}
	})
})

//
// product import form
//

app.all('/import', function(req, res) {
	let html = fm.renderSync('import-form.html', {})
	let pageHTML = fm.renderSync('framework.html', {title:'Kraft Import', body:html})
	res.end(pageHTML)
})

app.all('/import/submit', function(req, res) {
	let config = {
		brands:req.body.brands ? req.body.brands.split(',') : [],
		products:req.body.products ? req.body.products.split(',') : [],
		recipes:req.body.recipes ? req.body.recipes.split(',') : [],
		filename:req.body.filename || null,
		options:{
			cache:Boolean(req.body.cache)
		}
	}
	toXLS.importKraftData(config,function(resp){
		let html = fm.renderSync('import-form.html', resp)
		let pageHTML = fm.renderSync('framework.html', {title:'Kraft Import', body:html})
		res.end(pageHTML)
	})
})

if(process.env.PORT){
	app.listen(process.env.PORT, function() {
		console.log('Node app is running on port', app.get('port'))
	})
} else {
	openport.find({ startingPort: 5000 }, function(err, port) {
	    if (err) {
	        console.log(err)
	    } else {
	        app.listen(port, function() {
	            console.log('Running on port', port)
	            
	            let browser = (process.platform === "win32") ? "chrome" : "google chrome";
  				opn(`http://localhost:${port}/`, { app: [browser] })
	        })
	    }
	})
}