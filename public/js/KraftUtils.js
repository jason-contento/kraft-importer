var KraftUtils = (function(){

	function getAllProducts(cb){
		getBrandProducts(cb)
	}

	function getBrandProducts(cb){
		$.ajax({
			url:'/api/kraft/brands',
			data:{
				products:1
			},
			type:'GET',
			dataType:'json',
			success:cb
		})
	}

	return {
		getBrandProducts:getBrandProducts,
		getAllProducts:getAllProducts
	}

})()