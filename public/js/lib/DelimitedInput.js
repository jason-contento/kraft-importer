/*

textarea.delimited {
    resize:none; 
    overflow: hidden; 
    white-space: nowrap;
}

<textarea class="form-control delimited" rows="1" placeholder="Enter items, comma separated..."></textarea>


*/
function DelimitedInput(input){

    if(typeof input ==='string'){
        input = document.querySelector(input)
    }

    function trim(str) {
        return str.replace(/^\s+|\s+$/g, '');
    }

    function getDelimitedItems(str){
        return String(trim(str)).replace(/[\,\||;]/g,' ').split(/\s{1,}/g).filter(function(e){
            return trim(e)
        })
    }

    function fixDelimitedField(str){
        return getDelimitedItems(str).join(', ')
    }

    function handlePaste(evt){
        evt.target.value = evt.target.value+' '
        setTimeout(function(){
            evt.target.value = fixDelimitedField(evt.target.value.replace(/(?:\r\n|\r|\n|\||\s)/g,', '))
        },1)
    }

    function handleBlur(evt){
        this.value = fixDelimitedField(this.value)
    }

    function handleKey(evt){
        //console.log(evt)
//
        // setTimeout(function(){
            if(evt.keyCode === 13){
                evt.target.value = evt.target.value.replace(/(\r?\n|\r\n?)/g, "")
            }
       // })
        return false;
    }

    function getItems(){
        return getDelimitedItems(input.value)
    }

    function init(){
        var els = document.querySelectorAll('textarea.delimited')
        var i = els.length

        while(i--){
            initField(els[i])
        }
    }

    function initField(el){
        if(!el.classList.contains('delimited-initialized')){
            el.addEventListener('paste', handlePaste)
            el.addEventListener('blur', handleBlur)
            el.addEventListener('keydown', handleKey)
            el.classList.add('delimited-initialized')
            handleBlur.call(el)
        }
    }

    return {
        init:function(){
            if(!input){
                return init()
            }
            return initField(input)
        },
        val:function(){
            return getItems(input)
        }
    }
}

/*

init delimited inputs on load

*/  
if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
    DelimitedInput().init()
} else {
    document.addEventListener('DOMContentLoaded', DelimitedInput().init)
}

// (function(){
//     /*

//     init delimited inputs on load

//     */
//     function initDelimitedInputs(){
//         console.log('initDelimitedInputs')
//         var els = document.querySelectorAll('textarea.delimited')
//         var i = els.length
//         while(i--){
//             DelimitedInput(els[i]).init()
//         }
//     }

//     if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
//         initDelimitedInputs()
//     } else {
//         document.addEventListener('DOMContentLoaded', initDelimitedInputs)
//     }

// })()
