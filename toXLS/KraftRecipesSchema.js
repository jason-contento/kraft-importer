var UTC_DAY = 24*60*60*1000
var starttime = 0//new Date().getTime()
var endtime = 0//new Date(new Date().getTime()+(UTC_DAY*365*10)).getTime()

var custom_schema = {
	categorymaster: {
		categoryid: '',
		parentcategoryid: 'top',
		status: 1,
		hasproducts: true,
		defaultparentcategory: true,
		visible: true,
		locked: false,
		starttime: starttime,
		endtime: endtime,
		type: 0
	},
	categorymasterproperties: {
		categoryid: '',
		name: '',
		value: '',
		locale: 'en_US',
		locked: false,
		starttime: starttime,
		endtime: endtime
	},
	productmaster: {
		productid: function(entry){
			return entry.RecipeId
		},
		status: 1,
		collection: true,
		subproductids: function(entry){
			if(entry.RiseIngredients){
				return entry.RiseIngredients.filter(function(ingredient){
					return ingredient.ProductId
				}).map(function(ingredient){
					return ingredient.ProductId
				}).join()
			} else {
				return ''
			}
		},
		upsellproductids: '',
		crosssellproductids: function(entry){
			/*
RelatedData": {
        "RelatedBrands": null,
        "RelatedProducts": null,
        "RelatedRecipes": {
          "DefaultTopCount": 5,
          "Recipe": [
            {
              "AverageRating": 4.5,
              "HasVideo": true,
              "IsHealthyLiving": true,
              "NumberOfRatings": 175,
              "PhotoName": "Sangria_Punch.jpg",
              "PhotoUrl": "http://assets.kraftfoods.com/recipe_images/opendeploy/Sangria_Punch.jpg",
              "RecipeId": 64881,
              "RecipeName": "Sangria Punch",
              "SEOName": "sangria-punch-64881",
              "ThumbnailPhoto": null
			*/
			try {
				entry.RelatedData.RelatedRecipes.Recipe.map(function(recipe){
					return recipe.RecipeId
				})
			} catch(err) {
				return ''
			}
		},
		bundle: '',
		bundlemainproductid: '',
		bundlemandatoryproductids: '',
		bundleoptionalproductids: '',
		defaultparentcategoryid: function(entry){
			return entry.BrandID ? entry.BrandID : (entry.Brands && entry.Brands.length ? entry.Brands[0].id : 'recipes')
		},
		groupid: '',
		facetgroup: '',
		starttime: starttime,
		endtime: endtime,
		visible: true,
		locked: false
	},
	productmasterproperties: {
		productid: function(entry){
			return entry.RecipeId
		},
		name: '',
		value: '',
		locked: false,
		locale: 'en_US',
		overridebysku: false,
	    starttime: 0,
	    endtime: 0
	},
	skumaster: {
		skuid: function(entry){
			return entry.RecipeId
		},
		productids: function(entry){
			return entry.RecipeId
		},
		upcids: function(entry){
			return entry.RecipeId
		},
		status: 1,
		locked: false,
	    starttime: starttime,
	    endtime: endtime
	},
	skumasterproperties: {
		skuid: function(entry){
			return entry.ID
		},
		name: '',
		value: '',
		locked: false,
		locale: 'en_US',
	    starttime: starttime,
	    endtime: endtime
	},
	productcategory: {
		categoryid: function(entry){
			return entry.CategoryID
		},
		productid: function(entry){
			return entry.RecipeId
		},
		status: 1,
		starttime: starttime,
		endtime: endtime
	},
	facetmaster: {
		name: '',
		locale: 'en_US',
		type: 0,
		fieldtype: 0,
		validationtype: 0,
		displaytype: 0,
		filterable: true,
		searchable: true,
		sortable: false,
		required: false,
		status: 1,
	    starttime: starttime,
		endtime: endtime
	},
	facetmasterproperties: {
		facetname: '',
		facetlocale:'en_US',
		name: '',
		value: '',
		locale:'en_US',	
		locked:false,
	    starttime: starttime,
		endtime: endtime
	},
	storemaster: {
		storeId: 1,
		parentstoreid: 2,
		status: 3,
		type: 4,
		zip: 5,
		latitude: 6,
		longitude: 7,
		startTime: 8,
		endTime: 9
	},
	storemasterproperties: {
		storeId: 1,
		name: 2,
		value: 3,
		locale: 4,
		locked: 5,
		starttime: 6,
		endtime: 7
	},
	storesku: {
		storeid: 1,
		skuid: 2,
		status: 3,
		locked: 4
	},
	storeskuproperties: {
		storeid: 1,
		skuid: 2,
		name: 3,
		value: 4,
		locked: 5,
		locale: 6,
		starttime: 7,
		endtime: 8
	},
	storecategory: {
		storeid: 1,
		categoryid: 2,
		status: 3,
		locked: 4
	},
	storecategoryproperties: {
		storeid: 1,
		categoryid: 2,
		name: 3,
		value: 4,
		locale: 5,
		locked: 6,
		starttime: 7,
		endtime: 8
	},
	storeproduct: {
		storeid: 1,
		productid: 2,
		status: 3,
		locked: 4
	},
	storeproductproperties: {
		storeid: 1,
		productid: 2,
		name: 3,
		value: 4,
		locked: 5,
		locale: 6,
		starttime: 7,
		endtime: 8
	}
}

module.exports = custom_schema